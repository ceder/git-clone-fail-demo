#!/bin/sh
set -e
cd test-git/committer
i=0
while :
do
    echo $i > foo
    git add foo
    git commit -m"Updated foo"
    git push
    i=`expr $i + 1`
done
