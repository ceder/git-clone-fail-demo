#!/bin/sh
mkdir test-git || exit 1
cd test-git

if [ x"$1" = x"--use-ssh" ]
then
    touch use_ssh
    use_ssh=ssh://localhost/`pwd`/
else
    use_ssh=
fi

mkdir master.git
(cd master.git && git init --bare)
git clone ${use_ssh}master.git committer
(cd committer &&
    touch foo &&
    git add foo &&
    git commit -m"Initial commit" &&
    git push origin master)
git clone --mirror ${use_ssh}master.git mirror.git
