#!/bin/sh

what=$1

case "$what" in
    master);;
    mirror);;
    *) echo need to specify master or mirror >&2
       exit 1;;
esac

good=0
cd test-git || exit 1
rm -rf $what-clone

if [ -f use_ssh ]
then
    use_ssh=ssh://localhost/`pwd`/
else
    use_ssh=
fi


while :
do
    git clone ${use_ssh}$what.git $what-clone >$what.out 2>$what.err || exit 1
    if [ -s $what.err ]
    then
	echo unexpected stderr from git clone:
	cat $what.err
	exit 1
    fi
    found=false
    for expected in ../stdout.$what.*.txt
    do
	if cmp -s $expected $what.out
	then
	    found=true
	fi
    done
    if ! $found
    then
	echo unexpected stdout from git clone:
	cat $what.out
	exit 1
    fi
    if ! [ -f $what-clone/foo ]
    then
	echo failed to create $what-clone/foo
	exit 1
    fi
    rm -rf $what-clone
    good=`expr $good + 1`
    echo "$good good clones"
done
